require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    subject { get :show, params: { id: product.id } }

    let(:taxon)            { create(:taxon) }
    let(:product)          { create(:product, taxons: [taxon]) }
    let(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    # 正常にレスポンスを返すか
    it "responds successfully" do
      subject
      expect(response).to be_successful
    end

    # show.html.erbが描画されているか
    it "render show view" do
      subject
      expect(response).to render_template :show
    end

    # 正しくproductが渡されているか
    it "has correct product" do
      subject
      expect(assigns(:product)).to eq(product)
    end

    # 正しくrelated_productsが渡され,表示制限も適用されているか
    it "has correct related_products and displaying" do
      subject
      expect(assigns(:related_products)).to match_array(related_products[0..3].sort)
    end
  end
end
