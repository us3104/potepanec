require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon)    { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product1) { create(:product, taxons: [taxon]) }
  let!(:product2) { create(:product) }

  before { visit potepan_category_path(taxon.id) }

  # categories/showページの要素が期待する情報を所有している
  scenario "User accesses a expected show page" do
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content "(#{taxon.all_products.count})"
    end

    within '.productBox' do
      expect(page).to have_link product1.name, href: potepan_product_path(product1.id)
      expect(page).to have_content product1.display_price
      expect(page).not_to have_link product2.name, href: potepan_product_path(product2.id)
    end
  end

  # categories/showページの商品名から、products/showページにアクセスできる。
  scenario "User accesses products/show  from product name in categories/show" do
    click_link product1.name

    expect(page).to have_current_path potepan_product_path(product1.id)
  end
end
