class Potepan::ProductsController < ApplicationController
  DISPLAY_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product).asc.limit(DISPLAY_RELATED_PRODUCTS)
  end
end
